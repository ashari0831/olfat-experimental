<?php

test('user can be created', function () {
    $this->postJson(route('user.store', [
        'id' => 'ada63f73-6c1d-41db-a55c-816f05c8bfb3',
        'first_name' => 'firstName',
        'last_name' => 'lastName',
    ]));

    $this->assertDatabaseHas('users', [
        'id' => 'ada63f73-6c1d-41db-a55c-816f05c8bfb3',
        'first_name' => 'firstName',
        'last_name' => 'lastName',
    ]);
});
