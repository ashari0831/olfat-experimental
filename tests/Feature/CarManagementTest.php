<?php

test('car can be created', function () {
    $this->postJson(route('car.store', [
        'id' => 'ada63f73-6c1d-41db-a55c-816f05c8bfb3',
        'brand' => 'BMW',
        'model' => 'modelSample',
    ]));

    $this->assertDatabaseHas('cars', [
        'id' => 'ada63f73-6c1d-41db-a55c-816f05c8bfb3',
        'brand' => 'BMW',
        'model' => 'modelSample',
    ]);
});
