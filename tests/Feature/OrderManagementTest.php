<?php
use App\Models\Car;
use App\Models\Order;
use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;

test('order can be created', function () {
    $user = User::factory()->create();
    $car = Car::factory()->create();

    $this->postJson(route('orders.store', [
        'user_id' => $user->id,
        'car_id' => $car->id,
    ]));

    $this->assertDatabaseHas('orders', [
        'user_id' => $user->id,
        'car_id' => $car->id,
    ]);
});

test('order can be shown', function () {
    $user = User::factory()->create();
    $car = Car::factory()->create();
    $order = Order::factory()->create([
        'user_id' => $user->id,
        'car_id' => $car->id,
    ]);

    $response = $this->getJson(route('orders.show', $order->id));

    $response
        ->assertOk()
        ->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'data',
                fn(AssertableJson $json) => $json
                    ->where('id', $order->id)
                    ->where('user_id', $user->id)
                    ->where('car_id', $car->id)
                    ->has(
                        'user',
                        fn(AssertableJson $json) => $json
                            ->where('id', $user->id)
                            ->where('first_name', $user->first_name)
                            ->where('last_name', $user->last_name)
                            ->etc()
                    )
                    ->has(
                        'car',
                        fn(AssertableJson $json) => $json
                            ->where('id', $car->id)
                            ->where('brand', $car->brand)
                            ->where('model', $car->model)
                            ->etc()
                    )
                    ->etc()
            )
        );
});

test('order can be updated', function () {
    $order = Order::factory()->create();
    $newRegisteredUser = User::factory()->create();

    $response = $this->putJson(route('orders.update', $order->id), [
        'user_id' => $newRegisteredUser->id,
        'car_id' => $order->car_id,
    ]);

    $response
        ->assertOk()
        ->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'data',
                fn(AssertableJson $json) => $json
                    ->where('id', $order->id)
                    ->where('user_id', $newRegisteredUser->id)
                    ->where('car_id', $order->car_id)
                    ->etc()
            )
        );

    $this->assertDatabaseHas('orders', [
        'user_id' => $newRegisteredUser->id,
        'car_id' => $order->car_id,
    ]);
});

test('order can be deleted', function () {
    $order = Order::factory()->create();

    $response = $this->deleteJson(route('orders.destroy', $order->id));

    $response->assertNoContent();

    $this->assertDatabaseMissing('orders', [
        'id' => $order->id,
        'user_id' => $order->user_id,
        'car_id' => $order->car_id,
    ]);
});

test('orders can be listed by different filters', function ($queryParamName, $queryParamValue) {
    $user = User::factory()->create([
        'first_name' => 'name_sample',
    ]);
    $car = Car::factory()->create([
        'id' => '75cdb607-3214-43de-82f4-b3550d29c6dc',
        'brand' => 'brand_sample',
    ]);
    $order = Order::factory()->create([
        'user_id' => $user->id,
        'car_id' => $car->id,
    ]);
    Order::factory()->create();

    $response = $this->getJson(route('orders.index', [$queryParamName => $queryParamValue]));

    $response
        ->assertOk()
        ->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'data',
                1,
                fn(AssertableJson $json) => $json
                    ->where('id', $order->id)
                    ->where('user_id', $user->id)
                    ->where('car_id', $car->id)
                    ->has(
                        'user',
                        fn(AssertableJson $json) => $json
                            ->where('id', $user->id)
                            ->where('first_name', $user->first_name)
                            ->where('last_name', $user->last_name)
                            ->etc()
                    )
                    ->has(
                        'car',
                        fn(AssertableJson $json) => $json
                            ->where('id', $car->id)
                            ->where('brand', $car->brand)
                            ->where('model', $car->model)
                            ->etc()
                    )
                    ->etc()
            )
                ->etc()
        );
})
    ->with([
        'filter by car_id' => ['car_id', '75cdb607-3214-43de-82f4-b3550d29c6dc'],
        'filter by user first_name' => ['first_name', 'name_sample'],
        'filter by car brand' => ['brand', 'brand_sample'],
    ]);
;


