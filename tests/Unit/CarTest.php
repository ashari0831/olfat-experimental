<?php
use App\Models\Car;
use Illuminate\Testing\Fluent\AssertableJson;

test('car can be created', function () {
    $carBrand = 'BMW';
    $carModel = 'model_sample';
    $carId = '3ff6a3d3-958f-48de-85df-f15b828301ed';

    $mockedCar = Mockery::mock(Car::class);

    $mockedCar->shouldReceive('create')
        ->andReturn([
            'id' => $carId,
            'brand' => $carBrand,
            'model' => $carModel,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

    $this->app->instance(Car::class, $mockedCar);

    $response = $this->postJson(route('car.store', [
        'id' => $carId,
        'brand' => $carBrand,
        'model' => $carModel,
    ]));

    $response
        ->assertOk()
        ->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'data',
                fn(AssertableJson $json) =>
                $json
                    ->where('id', $carId)
                    ->where('brand', $carBrand)
                    ->where('model', $carModel)
                    ->etc()
            )
        );
});
