<?php
use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;

test('user can be created', function () {
    $userFirstName = 'first_name_sample';
    $userLastName = 'last_name_sample';
    $userId = '3ff6a3d3-958f-48de-85df-f15b828301ed';

    $mockedUser = Mockery::mock(User::class);

    $mockedUser->shouldReceive('create')
        ->andReturn([
            'id' => $userId,
            'first_name' => $userFirstName,
            'last_name' => $userLastName,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

    $this->app->instance(User::class, $mockedUser);

    $response = $this->postJson(route('user.store', [
        'id' => $userId,
        'first_name' => $userFirstName,
        'last_name' => $userLastName,
    ]));

    $response
        ->assertOk()
        ->assertJson(
            fn(AssertableJson $json) =>
            $json->has(
                'data',
                fn(AssertableJson $json) =>
                $json
                    ->where('id', $userId)
                    ->where('first_name', $userFirstName)
                    ->where('last_name', $userLastName)
                    ->etc()
            )
        );
});
