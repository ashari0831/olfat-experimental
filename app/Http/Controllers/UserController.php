<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Models\User;

class UserController extends Controller
{
    public function store(StoreUserRequest $request)
    {
        $user = app(User::class)->create($request->validated());

        return response()->json([
            'data' => $user,
        ]);
    }
}
