<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Order;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class OrderController extends Controller
{
    public function store(StoreOrderRequest $request)
    {
        $order = app(Order::class)->create($request->validated());

        return response()->json([
            'data' => $order,
        ]);
    }

    public function index(Request $request)
    {
        $orders =
            Order::filter($request)
                ->with(['user', 'car'])
                ->paginate($request->input('per_page', 10));

        return response()->json($orders);
    }

    public function show(Order $order)
    {
        $order->load(['user', 'car']);

        return response()->json([
            'data' => $order,
        ]);
    }

    public function update(UpdateOrderRequest $request, Order $order)
    {
        $order->update($request->validated());

        return response()->json([
            'data' => $order->refresh(),
        ]);
    }

    public function destroy(Order $order)
    {
        $order->delete();

        return response()->json([], 204);
    }
}
