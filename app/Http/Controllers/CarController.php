<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCarRequest;
use App\Models\Car;

class CarController extends Controller
{
    public function store(StoreCarRequest $request)
    {
        $car = app(Car::class)->create($request->validated());

        return response()->json([
            'data' => $car,
        ]);
    }
}
