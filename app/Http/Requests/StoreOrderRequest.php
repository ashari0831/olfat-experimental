<?php

namespace App\Http\Requests;

use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Closure;


class StoreOrderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $uuid4Regex = "/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i";

        return [
            'user_id' => [
                'required',
                "regex:{$uuid4Regex}",
                Rule::exists('users', 'id'),
                function (string $attribute, mixed $value, Closure $fail) {
                    if (
                        Order::where('user_id', $value)->where('car_id', $this->car_id)->exists()
                    )
                        $fail("The user_id car_id pair has been taken.");
                },
            ],
            'car_id' => ['required', "regex:{$uuid4Regex}", Rule::exists('cars', 'id')],
        ];
    }
}
