<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = [
        'user_id',
        'car_id',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function car(): BelongsTo
    {
        return $this->belongsTo(Car::class);
    }

    public function scopeFilter($query, $request)
    {
        if ($request->input('user_id')) {
            $query->where('user_id', $request->input('user_id'));
        }

        if ($request->input('car_id')) {
            $query->where('car_id', $request->input('car_id'));
        }

        if ($request->input('first_name')) {
            $query->whereHas('user', fn($query) => $query->where('first_name', $request->input('first_name')));
        }

        if ($request->input('last_name')) {
            $query->whereHas('user', fn($query) => $query->where('last_name', $request->input('last_name')));
        }

        if ($request->input('brand')) {
            $query->whereHas('car', fn($query) => $query->where('brand', $request->input('brand')));
        }

        if ($request->input('model')) {
            $query->whereHas('car', fn($query) => $query->where('model', $request->input('model')));
        }

        return $query;
    }
}
